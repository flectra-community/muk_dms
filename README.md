# Flectra Community / muk_dms

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[muk_dms_field](muk_dms_field/) | 1.0.2.0.6| Document Fields
[muk_dms_share](muk_dms_share/) | 1.0.2.0.1| Share Documents
[muk_dms_access](muk_dms_access/) | 1.0.2.0.1| Access Control
[muk_dms_attachment](muk_dms_attachment/) | 1.0.2.0.4| Documents as Attachment Storage
[muk_dms_lobject](muk_dms_lobject/) | 1.0.2.0.1| Document Large Object Support
[muk_dms](muk_dms/) | 1.0.2.1.9| Document Management System
[muk_dms_export](muk_dms_export/) | 1.0.1.0.1| Export Files
[muk_dms_file](muk_dms_file/) | 1.0.2.0.10| Document File System Support
[muk_dms_thumbnails](muk_dms_thumbnails/) | 1.0.1.0.1| Automatic File Thumbnails


